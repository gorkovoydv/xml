<?php
require_once "vendor/autoload.php";

use ViskeySound\Xml\Entity;
use ViskeySound\Xml\Generator;
use ViskeySound\Xml\XmlNamespace;

$xmlTemplate = [
    new Entity('Header'),
    new Entity(
        'Body',
        [
            new Entity(
                'GetListDocuments',
                [
                    new Entity('document', '1'),
                    new Entity('document', '2'),
                    new Entity('document', '3'),
                ],
                new XmlNamespace('tem', 'http://tempuri.org/')
            ),
        ]
    ),
];

$xml = new \SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?><documents></documents>");

$creator = new Generator($xml);
$creator->create($xmlTemplate);

echo $xml->asXML();