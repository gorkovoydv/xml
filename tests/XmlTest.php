<?php

namespace ViskeySound\Xml\Test;

use ViskeySound\Xml\Entity;
use ViskeySound\Xml\Generator;
use ViskeySound\Xml\XmlNamespace;

class XmlTest extends \PHPUnit_Framework_TestCase
{
    public function testXmlGenerator()
    {
        $expectedXml = '<?xml version="1.0" encoding="UTF-8"?>
<documents><Header xmlns=""/><Body xmlns=""><tem:GetListDocuments xmlns:tem="http://tempuri.org/"><document xmlns="">1</document><document xmlns="">2</document><document xmlns="">3</document></tem:GetListDocuments></Body></documents>';

        $xmlTemplate = [
            new Entity('Header'),
            new Entity(
                'Body',
                [
                    new Entity(
                        'GetListDocuments',
                        [
                            new Entity('document', '1'),
                            new Entity('document', '2'),
                            new Entity('document', '3'),
                        ],
                        new XmlNamespace('tem', 'http://tempuri.org/')
                    ),
                ]
            ),
        ];

        $xml = new \SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?><documents></documents>");

        $creator = new Generator($xml);
        $creator->create($xmlTemplate);

        $this->assertXmlStringEqualsXmlString($expectedXml, $xml->asXML());
    }
}
