## Install

Via Composer

``` bash
$ composer require gorkovoy/xml
```

## Usage

``` php

$xmlTemplate = [
    new Entity('Header'),
    new Entity(
        'Body',
        [
            new Entity(
                'list',
                [
                    new Entity('document', '1'),
                    new Entity('document', '2'),
                    new Entity('document', '3'),
                ],
                new XmlNamespace('tem', 'http://tempuri.org/')
            ),
        ]
    ),
];

$xml = new \SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?><documents></documents>");

$creator = new Generator($xml);
$creator->create($xmlTemplate);

echo $xml->asXML();
```

## Testing

``` bash
$ phpunit tests
```

## Credits

- [Gorkovoy Dmitriy](https://bitbucket.org/gorkovoydv)


## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
