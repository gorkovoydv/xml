<?php

namespace ViskeySound\Xml;

/**
 * Class XmlNamespace
 * @package ViskeySound\Xml
 */
class XmlNamespace
{
    /**
     * имя нэймспэйса
     * @var null
     */
    protected $name;

    /**
     * префикс в тег
     * @var string
     */
    protected $prefix;

    /**
     * XmlNamespace constructor.
     * @param $prefix
     * @param $name
     */
    public function __construct(string $prefix = '', $name = '')
    {
        $this->name = $name;
        $this->prefix = !empty($prefix) ? $prefix . ':' : $prefix;
    }

    /**
     * @return mixed
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPrefix(): string
    {
        return $this->prefix;
    }
}
