<?php

namespace ViskeySound\Xml;

/**
 * Class Generator
 * @package ViskeySound\Xml
 */
class Generator
{
    /**
     * @var \SimpleXMLElement
     */
    private $xml;

    /**
     * Generator constructor.
     * @param \SimpleXMLElement $xml
     */
    public function __construct(\SimpleXMLElement $xml)
    {
        $this->xml = $xml;
    }

    /**
     * @return \SimpleXMLElement
     */
    public function getXml(): \SimpleXMLElement
    {
        return $this->xml;
    }

    /**
     * @param Entity $entity
     * @return \SimpleXMLElement
     */
    protected function createItem(Entity $entity)
    {
        if (is_array($entity->getValue())) {
            $child = $this->getXml()->addChild($entity->getName(), null, $entity->getNamespace());

            foreach ($entity->getValue() as $entityItem) {
                (new Generator($child))->createItem($entityItem);
            }
        } else {
            $child = $this->getXml()->addChild($entity->getName(), $entity->getValue(), $entity->getNamespace());
        }

        foreach ($entity->getAttributes() as $keyAttr => $valAttr) {
            $child->addAttribute($keyAttr, $valAttr);
        }

        return $child;
    }

    /**
     * @param array $items
     */
    public function create(array $items)
    {
        foreach ($items as $item) {
            $this->createItem($item);
        }
    }
}
