<?php

namespace ViskeySound\Xml;

/**
 * Class Entity
 * @package ViskeySound\Xml
 */
class Entity
{
    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * @var mixed
     */
    protected $value;

    /**
     * @var mixed
     */
    protected $name;

    /**
     * @var XmlNamespace
     */
    protected $namespace;

    /**
     * Entity constructor.
     * @param string $name
     * @param mixed $value
     * @param null|XmlNamespace $namespace
     */
    public function __construct(string $name = '', $value = '', $namespace = null)
    {
        $this->value = $value;
        $this->name = $name;
        $this->namespace = $namespace ?? new XmlNamespace();
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setValue($value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName(): string
    {
        return $this->namespace->getPrefix() . $this->name;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    public function getNamespace(): string
    {
        return $this->namespace->getName();
    }
}
